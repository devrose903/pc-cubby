create table users
(
    id       INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
    active   BOOLEAN DEFAULT true,
    name     VARCHAR(155) NOT NULL,
    login    VARCHAR(100) NOT NULL,
    mail     VARCHAR(100) NOT NULL,
    password TEXT         NULL,
    created  timestamp default now(),
    modified timestamp default now(),
    UNIQUE KEY users_login_uindex (login)
);

create table roles
(
    id          INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name        VARCHAR(155) NOT NULL,
    description TEXT         NULL,
    UNIQUE KEY roles_name_uindex (name)
);

create table users_roles
(
    user_id INT NOT NULL,
    role_id INT NOT NULL,
    PRIMARY KEY (user_id, role_id),
    CONSTRAINT fk_users_roles_role_id FOREIGN KEY (role_id) REFERENCES roles (id),
    CONSTRAINT fk_users_roles_user_id FOREIGN KEY (user_id) REFERENCES users (id)
);

# inne:

create table hardware_details
(
    id               INT  NOT NULL AUTO_INCREMENT PRIMARY KEY,
    description      TEXT,
    buy_date         date,
    active_guarantee BOOLEAN   DEFAULT false,
    guarantee_years  INT       DEFAULT 0,
    serial_num       VARCHAR(100) NOT NULL,
    invoice_num      VARCHAR(100),
    shop_url         TEXT NULL,
    internal_num     VARCHAR(50)
);

create table hardware
(
    id           INT  NOT NULL AUTO_INCREMENT PRIMARY KEY,
    active       BOOLEAN DEFAULT true,
    name         TEXT NOT NULL,
    created      timestamp DEFAULT CURRENT_TIMESTAMP,
    modified     timestamp DEFAULT CURRENT_TIMESTAMP,
    type         VARCHAR(55),
    company_user VARCHAR(255),
    user_id      INT  NOT NULL,
    details_id   INT  NOT NULL,
    CONSTRAINT fk_hardware_details_id FOREIGN KEY (details_id) REFERENCES hardware_details (id),
    CONSTRAINT fk_hardware_user_id FOREIGN KEY (user_id) REFERENCES users (id),
    UNIQUE KEY hardware_name_user_idx (name, user_id),
    INDEX hardware_user_idx (user_id)
);

create table password_reset_token
(
    id INT  NOT NULL AUTO_INCREMENT PRIMARY KEY,
    token TEXT NOT NULL,
    user_id INT NOT NULL,
    expiry_date timestamp,
    CONSTRAINT fk_password_reset_token_user_id FOREIGN KEY (user_id) REFERENCES users (id)
);