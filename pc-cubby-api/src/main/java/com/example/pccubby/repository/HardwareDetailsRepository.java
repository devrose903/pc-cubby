package com.example.pccubby.repository;

import com.example.pccubby.entity.HardwareDetails;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HardwareDetailsRepository extends CrudRepository<HardwareDetails, Integer> {

}
