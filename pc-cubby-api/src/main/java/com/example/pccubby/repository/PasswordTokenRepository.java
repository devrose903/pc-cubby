package com.example.pccubby.repository;

import com.example.pccubby.entity.PasswordResetToken;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PasswordTokenRepository extends CrudRepository<PasswordResetToken, Integer> {
    PasswordResetToken findByToken(String token);

    @Query("SELECT prt FROM PasswordResetToken prt WHERE prt.user.id = :id")
    List<PasswordResetToken> findByUserId(@Param("id")Integer userId);
}
