package com.example.pccubby.repository;

import com.example.pccubby.entity.Hardware;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface HardwareRepository extends JpaRepository<Hardware, Integer> {
    @Query("SELECT h, hd FROM Hardware h JOIN HardwareDetails hd ON h.details.id = hd.id WHERE h.id = :id")
    Optional<Hardware> findByIdWithDetails(@Param("id") int id);

    @Query("SELECT h, hd FROM Hardware h JOIN HardwareDetails hd ON h.details.id = hd.id WHERE h.id = :id and h.user.id = :userId")
    Optional<Hardware> findByIdWithDetailsByUser(@Param("id")int id, @Param("userId") int userId);

    @Query("SELECT h FROM Hardware h JOIN HardwareDetails hd ON h.details.id = hd.id WHERE h.id = :id and h.user.id = :userId")
    Optional<Hardware> findByIdAndUser(@Param("id")int id, @Param("userId") int userId);
}
