package com.example.pccubby.repository;

import com.example.pccubby.entity.Role;
import com.example.pccubby.entity.enums.RoleEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {
    Optional<Role> findByName(RoleEnum roleEnum);
}
