package com.example.pccubby.repository;

import com.example.pccubby.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    Optional<User> findByLogin(@Param("login") String login);

    @Query("SELECT u FROM User u WHERE u.login = :login AND u.active = true")
    Optional<User> findByLoginAndActive(@Param("login") String login);

    boolean existsByLogin(@Param("login") String login);
}
