package com.example.pccubby.control;

import com.example.pccubby.boundary.request.HardwareFilter;
import com.example.pccubby.boundary.request.HardwareRequest;
import com.example.pccubby.boundary.response.ApiResponse;
import com.example.pccubby.boundary.response.HardwareListResponse;
import com.example.pccubby.boundary.response.HardwareResponse;
import com.example.pccubby.entity.Hardware;
import com.example.pccubby.entity.HardwareDetails;
import com.example.pccubby.entity.User;
import com.example.pccubby.entity.enums.HardwareTypeEnum;
import com.example.pccubby.repository.HardwareDetailsRepository;
import com.example.pccubby.repository.HardwareRepository;
import com.example.pccubby.repository.UserRepository;
import com.example.pccubby.security.services.UserDetailsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/hardware")
public class HardwareController {
    private static final Logger logger = LoggerFactory.getLogger(HardwareController.class);

    final HardwareRepository hardwareRepository;
    final UserRepository userRepository;
    final HardwareDetailsRepository hardwareDetailsRepository;

    public HardwareController(HardwareRepository hardwareRepository, UserRepository userRepository, HardwareDetailsRepository hardwareDetailsRepository) {
        this.hardwareRepository = hardwareRepository;
        this.userRepository = userRepository;
        this.hardwareDetailsRepository = hardwareDetailsRepository;
    }

    @PostMapping("/list")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> getHardwareList(@RequestBody HardwareFilter filter) {
        List<Hardware> hardwares = new ArrayList<>();
        String name = filter.getName();
        String companyUser = filter.getCompanyUser();

        if (name != null) {
            HtmlUtils.htmlEscape(name);
        }
        if (companyUser != null) {
            HtmlUtils.htmlEscape(companyUser);
        }

        ExampleMatcher matcher = ExampleMatcher.matchingAll()
                .withMatcher("name", ExampleMatcher.GenericPropertyMatcher.of(ExampleMatcher.StringMatcher.CONTAINING).ignoreCase())
                .withMatcher("companyUser", ExampleMatcher.GenericPropertyMatcher.of(ExampleMatcher.StringMatcher.CONTAINING).ignoreCase());

        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();

            if (auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMIN"))) {
                logger.info("Searching all hardwares for admin");
                Hardware hardware = new Hardware();

                User user = new User();
                user.setActive(true);
                hardware.setUser(user);
                hardware.setName(name);
                hardware.setCompanyUser(companyUser);

                hardwares.addAll(hardwareRepository.findAll(Example.of(hardware, matcher)));
            } else {
                logger.info("Searching all hardwares for user");
                UserDetailsImpl userImp = (UserDetailsImpl) auth.getPrincipal();

                Hardware hardwareUser = new Hardware();
                User user = new User();
                user.setActive(true);
                user.setId(userImp.getId());
                hardwareUser.setUser(user);
                hardwareUser.setName(name);
                hardwareUser.setCompanyUser(companyUser);

                hardwares.addAll(hardwareRepository.findAll(Example.of(hardwareUser, matcher)));
            }

            if (hardwares.isEmpty()) {
                logger.info("Found no hardwares");

                return ResponseEntity.ok().build();
            }

            logger.info("Found hardwares");

            List<HardwareListResponse> hardwareResponses;
            hardwareResponses = hardwares.stream().map(HardwareListResponse::new).collect(Collectors.toList());

            return ResponseEntity.ok(hardwareResponses);
        } catch (Exception e) {
            logger.error(e.getMessage());

            return new ResponseEntity<>(new ApiResponse("ERROR", "Wystąpił błąd pobierania listy"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/details/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Object getHardwareDetailsById(@PathVariable("id") Integer id) {
        logger.info("Searching hardwares by id");

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl user = (UserDetailsImpl) auth.getPrincipal();
        Optional<Hardware> detailsOpt;

        if (auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMIN"))) {
            logger.info("Searching hardware {} for admin", id);
            detailsOpt = hardwareRepository.findByIdWithDetails(id);
        } else {
            logger.info("Searching hardware {} for user", id);
            detailsOpt = hardwareRepository.findByIdWithDetailsByUser(id, user.getId());
        }

        if (detailsOpt.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("ERROR", "Nie znaleziono szczegółów"), HttpStatus.NOT_FOUND);
        }

        logger.info("Found one result");
        Hardware hardware = detailsOpt.get();
        HardwareDetails details = hardware.getDetails();

        return new HardwareResponse(hardware, details);
    }

    @GetMapping("/select/types")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<ArrayList<HashMap<String, String>>> getHardwareTypes() {

        ArrayList<HashMap<String, String>> options = new ArrayList<>();

        for (HardwareTypeEnum item : HardwareTypeEnum.values()) {
            HashMap<String, String> typeOptions = new HashMap<>();
            typeOptions.put("label", item.getValue());
            typeOptions.put("value", item.name());

            options.add(typeOptions);
        }

        return new ResponseEntity<>(options, HttpStatus.OK);
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> createHardware(@Valid @RequestBody HardwareRequest request) {
        logger.info("Creating hardware");

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userImpl = (UserDetailsImpl) auth.getPrincipal();
        Optional<User> userOpt = userRepository.findByLoginAndActive(userImpl.getUsername());

        logger.info("Checking if user exists");

        if (userOpt.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("ERROR", "Nie znaleziono aktywnego użytkownika"), HttpStatus.NOT_FOUND);
        }

        HardwareDetails details = new HardwareDetails();
        mapRequestToHardwareDetails(request, details);

        Hardware hardware = new Hardware(request.getName());
        hardware.setActive(request.getActive());
        hardware.setType(request.getType());
        hardware.setCompanyUser(request.getCompanyUser());
        hardware.setUser(userOpt.get());
        hardware.setDetails(details);

        try {
            logger.info("Saving new hardware item");
            hardwareDetailsRepository.save(details);
            hardwareRepository.save(hardware);

            return ResponseEntity.ok(new ApiResponse("OK", "Poprawnie utworzono sprzęt"));
        }  catch (DataIntegrityViolationException e) {
            logger.info("Error while saving data {}", e.getMessage());

            return new ResponseEntity<>(new ApiResponse("ERROR", "Już istnieje taki sprzęt"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            logger.error(e.getMessage());

            return new ResponseEntity<>(new ApiResponse("ERROR", "Wystąpił błąd"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/update")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> updateHardware(@Valid @RequestBody HardwareRequest request) {
        logger.info("Starting updating hardware..");

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userImpl = (UserDetailsImpl) auth.getPrincipal();

        logger.info("Checking if user exists");
        Optional<User> userOpt = userRepository.findByLoginAndActive(userImpl.getUsername());
        Optional<Hardware> hardwareOpt;

        if (userOpt.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("ERROR", "Nie znaleziono aktywnego użytkownika"), HttpStatus.NOT_FOUND);
        }

        logger.info("Finding hardware {}", request.getId());

        if (auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMIN"))) {
            hardwareOpt = hardwareRepository.findByIdWithDetails(request.getId());
        } else {
            hardwareOpt = hardwareRepository.findByIdWithDetailsByUser(request.getId(), userOpt.get().getId());
        }

        if (hardwareOpt.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("ERROR", "Nie znaleziono sprzętu"), HttpStatus.NOT_FOUND);
        }

        logger.info("Updating hardware and details entities");

        Hardware hardware = hardwareOpt.get();
        HardwareDetails details = hardware.getDetails();

        mapRequestToHardwareDetails(request, details);

        hardware.setName(request.getName());
        hardware.setActive(request.getActive());
        hardware.setType(request.getType());
        hardware.setCompanyUser(request.getCompanyUser());
        hardware.setDetails(details);

        try {
            logger.info("Saving hardware item");
            hardwareDetailsRepository.save(details);
            hardwareRepository.save(hardware);

            return ResponseEntity.ok(new ApiResponse("OK", "Poprawnie zaktualizowano wybrany sprzęt i jego szczegóły"));
        } catch (DataIntegrityViolationException e) {
            logger.info("Error while saving data {}", e.getMessage());

            return new ResponseEntity<>(new ApiResponse("ERROR", "Już istnieje taki sprzęt"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            logger.error(e.getMessage());

            return new ResponseEntity<>(new ApiResponse("ERROR", "Wystąpił błąd"),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private void mapRequestToHardwareDetails(@Valid @RequestBody HardwareRequest request, HardwareDetails details) {
        details.setDescription(request.getDescription());
        details.setActiveGuarantee(request.getActiveGuarantee());
        details.setGuaranteeYears(request.getGuaranteeYears());
        details.setBuyDate(request.getBuyDate());
        details.setSerialNum(request.getSerialNum());
        details.setInvoiceNum(request.getInvoiceNum());
        details.setInternalNum(request.getInternalNum());
        details.setMacAddress(request.getMacAddress());
        details.setShopUrl(request.getShopUrl());
    }

    @DeleteMapping("/remove/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> removeHardware(@PathVariable("id") Integer id) {
        Optional<Hardware> hardwareOptional;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMIN"))) {
            hardwareOptional = hardwareRepository.findById(id);
        } else {
            UserDetailsImpl userImpl = (UserDetailsImpl) auth.getPrincipal();
            hardwareOptional = hardwareRepository.findByIdAndUser(id, userImpl.getId());
        }

        if (hardwareOptional.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        try {
            Hardware hardware = hardwareOptional.get();
            HardwareDetails details = hardware.getDetails();

            logger.info("Removing hardware {}", id);
            hardwareRepository.deleteById(hardware.getId());

            logger.info("Removing hardware details {}", details.getId());
            hardwareDetailsRepository.deleteById(details.getId());

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());

            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}