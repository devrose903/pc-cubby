package com.example.pccubby.control;

import com.example.pccubby.boundary.request.UserEditRequest;
import com.example.pccubby.boundary.request.UserEditSimplifiedRequest;
import com.example.pccubby.boundary.request.UserRequest;
import com.example.pccubby.boundary.response.ApiResponse;
import com.example.pccubby.boundary.response.UserResponse;
import com.example.pccubby.boundary.response.UserSimplifiedResponse;
import com.example.pccubby.entity.Role;
import com.example.pccubby.entity.User;
import com.example.pccubby.entity.enums.RoleEnum;
import com.example.pccubby.repository.RoleRepository;
import com.example.pccubby.repository.UserRepository;
import com.example.pccubby.security.services.UserDetailsImpl;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/user")
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    final UserRepository userRepository;
    final RoleRepository roleRepository;

    PasswordEncoder passwordEncoder;

    public UserController(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/list")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getUserList() {
        List<User> users = new ArrayList<>();

        try {
            logger.info("Searching all users for admin");
            userRepository.findAll().forEach(users::add);

            if (users.isEmpty()) {
                logger.info("Found no users");

                return ResponseEntity.ok().build();
            }

            logger.info("Found users");

            List<UserResponse> userResponses;
            userResponses = users.stream().map(UserResponse::new).collect(Collectors.toList());

            return ResponseEntity.ok(userResponses);
        } catch (Exception e) {
            logger.error(e.getMessage());

            return new ResponseEntity<>(new ApiResponse("ERROR", "Wystąpił błąd"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> createUser(@Valid @RequestBody UserRequest request) throws NotFoundException {
        logger.info("Creating user..");

        if (userRepository.existsByLogin(request.getLogin())) {
            return new ResponseEntity<>(new ApiResponse("ERROR", "Już istnieje użytkownik o takim loginie"), HttpStatus.BAD_REQUEST);
        }

        User user = new User();
        user.setName(request.getName());
        user.setLogin(request.getLogin());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setMail(request.getMail());

        Role userRole = roleRepository.findByName(RoleEnum.ROLE_USER).orElseThrow(() -> new NotFoundException("Nie znaleziono roli"));
        user.setRoles(Collections.singleton(userRole));

        try {
            logger.info("Saving new user");
            userRepository.save(user);

            return ResponseEntity.ok(new ApiResponse("OK", "Poprawnie utworzono użytkownika"));
        } catch (DataIntegrityViolationException e) {
            logger.info("Error while saving data {}", e.getMessage());

            return new ResponseEntity<>(new ApiResponse("ERROR", "Użytkownik musi mieć unikalny login"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            logger.error(e.getMessage());

            return new ResponseEntity<>(new ApiResponse("ERROR", "Wystąpił błąd"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/info")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public UserSimplifiedResponse getSimplifiedUserData() throws NotFoundException {
        logger.info("Searching user by user");

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userImpl = (UserDetailsImpl) auth.getPrincipal();

        User loggedUser = userRepository.findByLoginAndActive(userImpl.getUsername()).orElseThrow(() -> new NotFoundException("Wystąpił błąd w trakcie szukania użytkownika"));

        return new UserSimplifiedResponse(loggedUser);
    }

    @PostMapping("/update")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> updateUser(@Valid @RequestBody UserEditRequest request) throws NotFoundException {
        logger.info("Updating user..");

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userImpl = (UserDetailsImpl) auth.getPrincipal();

        logger.info("Finding logged user");
        User loggedUser = userRepository.findByLoginAndActive(userImpl.getUsername()).orElseThrow(() -> new NotFoundException("Wystąpił błąd w trakcie szukania użytkownika"));

        logger.info("Finding user {}", request.getLogin());
        Optional<User> userOpt = userRepository.findByLogin(request.getLogin());

        if (userOpt.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("ERROR", "Wystąpił błąd przy edycji"), HttpStatus.BAD_REQUEST);
        }

        User user = userOpt.get();

        boolean isAdmin = auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMIN"));

        if (!isAdmin) {
            if (!user.getId().equals(loggedUser.getId())) {
                return new ResponseEntity<>(new ApiResponse("ERROR", "Brak uprawnień do edycji użytkownika"), HttpStatus.UNAUTHORIZED);
            }
        }

        user.setName(request.getName());
        user.setMail(request.getMail());

        try {
            logger.info("Saving user");
            userRepository.save(user);

            return ResponseEntity.ok(new ApiResponse("OK", "Poprawnie zaktualizowano użytkownika"));
        } catch (DataIntegrityViolationException e) {
            logger.info("Error while saving data {}", e.getMessage());

            return new ResponseEntity<>(new ApiResponse("ERROR", "Użytkownik musi mieć unikalny login"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            logger.error(e.getMessage());

            return new ResponseEntity<>(new ApiResponse("ERROR", "Wystąpił błąd"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/update/simply")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> updateSimplifiedUser(@Valid @RequestBody UserEditSimplifiedRequest request) throws NotFoundException {
        logger.info("Updating simplified user..");

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userImpl = (UserDetailsImpl) auth.getPrincipal();

        logger.info("Finding logged user");
        User loggedUser = userRepository.findByLoginAndActive(userImpl.getUsername()).orElseThrow(() -> new NotFoundException("Wystąpił błąd w trakcie szukania użytkownika"));

        logger.info("Finding user {}", request.getLogin());
        Optional<User> userOpt = userRepository.findByLogin(request.getLogin());

        if (userOpt.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse("ERROR", "Wystąpił błąd przy edycji"), HttpStatus.BAD_REQUEST);
        }

        User user = userOpt.get();

        boolean isAdmin = auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMIN"));

        if (!isAdmin) {
            if (!user.getId().equals(loggedUser.getId())) {
                return new ResponseEntity<>(new ApiResponse("ERROR", "Brak uprawnień do edycji użytkownika"), HttpStatus.UNAUTHORIZED);
            }
        }

        String password = request.getPassword();
        user.setName(request.getName());

        if (password != null) {
            if (password.length() >= 8) {
                user.setPassword(passwordEncoder.encode(request.getPassword()));
            } else {
                return new ResponseEntity<>(new ApiResponse("ERROR", "Hasło musi mieć przynajmniej 8 znaków"), HttpStatus.BAD_REQUEST);
            }
        }

        user.setMail(request.getMail());

        try {
            logger.info("Saving user");
            userRepository.save(user);

            return ResponseEntity.ok(new ApiResponse("OK", "Poprawnie zaktualizowano użytkownika"));
        } catch (DataIntegrityViolationException e) {
            logger.info("Error while saving data {}", e.getMessage());

            return new ResponseEntity<>(new ApiResponse("ERROR", "Użytkownik musi mieć unikalny login"), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            logger.error(e.getMessage());

            return new ResponseEntity<>(new ApiResponse("ERROR", "Wystąpił błąd"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/activate/{login}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> activateUser(@PathVariable("login") String login) {
        HtmlUtils.htmlEscape(login);
        Optional<User> userOptional = userRepository.findByLogin(login);

        if (userOptional.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        User user = userOptional.get();

        try {
            logger.info("Activating user {}", user.getId());
            user.setActive(true);
            userRepository.save(user);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());

            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/deactivate/{login}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> deactivateUser(@PathVariable("login") String login) {
        HtmlUtils.htmlEscape(login);
        Optional<User> userOptional = userRepository.findByLoginAndActive(login);

        if (userOptional.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        User user = userOptional.get();

        if (user.getRoles().stream().anyMatch(r -> r.getName().name().equals("ROLE_ADMIN"))) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        try {
            logger.info("Deactivating user {}", user.getId());
            user.setActive(false);
            userRepository.save(user);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage());

            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
