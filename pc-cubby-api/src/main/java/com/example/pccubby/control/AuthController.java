package com.example.pccubby.control;

import com.example.pccubby.boundary.request.LoginRequest;
import com.example.pccubby.boundary.request.ResetPasswordRequest;
import com.example.pccubby.boundary.response.ApiResponse;
import com.example.pccubby.boundary.response.JwtAuthResponse;
import com.example.pccubby.entity.PasswordResetToken;
import com.example.pccubby.entity.User;
import com.example.pccubby.repository.PasswordTokenRepository;
import com.example.pccubby.repository.RoleRepository;
import com.example.pccubby.repository.UserRepository;
import com.example.pccubby.security.services.UserDetailsImpl;
import com.example.pccubby.security.services.jwt.JwtTokenProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    final AuthenticationManager authenticationManager;
    final UserRepository userRepository;
    final RoleRepository roleRepository;
    final PasswordTokenRepository tokenRepository;
    final JwtTokenProvider tokenProvider;

    MailSender mailSender;
    PasswordEncoder passwordEncoder;

    public AuthController(AuthenticationManager authenticationManager, UserRepository userRepository, RoleRepository roleRepository, PasswordTokenRepository tokenRepository, JwtTokenProvider tokenProvider, MailSender mailSender, PasswordEncoder encoder) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.tokenRepository = tokenRepository;
        this.tokenProvider = tokenProvider;
        this.mailSender = mailSender;
        this.passwordEncoder = encoder;
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody LoginRequest request) {
        logger.info("Login in data");

        try {
            Authentication auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getLogin(), request.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(auth);
            String token = tokenProvider.generateToken(auth);

            UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();

            List<String> authorities = userDetails.getAuthorities().stream()
                    .map(GrantedAuthority::getAuthority)
                    .collect(Collectors.toList());

            return ResponseEntity.ok(new JwtAuthResponse(token, userDetails.getUsername(), authorities));
        } catch (BadCredentialsException e) {
            logger.error("Invalid login data");

            return new ResponseEntity<>(new ApiResponse("ERROR", "Nieprawidłowy login lub hasło"), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/reset-password/send/{login}")
    public ResponseEntity<?> resetPassword(@PathVariable("login") String login) {
            Optional<User> userOptional = userRepository.findByLogin(login);

            if (userOptional.isEmpty()) {
                return new ResponseEntity<>(new ApiResponse("ERROR", "Nieprawidłowy login"), HttpStatus.UNAUTHORIZED);
            }

            String token = UUID.randomUUID().toString();
            PasswordResetToken myToken = new PasswordResetToken(token, userOptional.get());

            try {
                this.mailSender.send(constructResetTokenEmail(token, userOptional.get()));
                this.tokenRepository.save(myToken);
                return ResponseEntity.ok(new ApiResponse("OK", "Poprawnie wysłano maila na adres przypisany dla danego loginu"));
            } catch (Exception e) {
                logger.error(e.getMessage());

                return new ResponseEntity<>(new ApiResponse("ERROR", "Wystąpił błąd"), HttpStatus.UNAUTHORIZED);
            }
    }

    private SimpleMailMessage constructResetTokenEmail(String token, User user) {
        String url = "http://pc-cubby.local:8001" + "/reset-password/" + token;
        String message = "Witaj! \r\n Otrzymujesz tę wiadomość, ponieważ dostaliśmy informację o resetowaniu hasła." +
                "\r\n Skorzystaj z podanego linka aby zresetować hasło (link jest ważny 24 godziny):";
        String lastMessage = "Jeśli nie jesteś adresatem tej wiadomości możesz ją zignorować. \r\n Pozdrawiamy zespół Pc Cubby";

        return constructEmail(message + " \r\n" + url + " \r\n \r\n" + lastMessage, user);
    }

    private SimpleMailMessage constructEmail(String body, User user) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setSubject("Resetowanie hasła Pc Cubby");
        email.setText(body);
        email.setTo(user.getMail());
        email.setFrom("info@pccubby.com");

        return email;
    }

    @PostMapping("/reset-password/update")
    public ResponseEntity<?> updateResetPassword(@Valid @RequestBody ResetPasswordRequest request) {
        String token = request.getToken();
        HtmlUtils.htmlEscape(token);

        PasswordResetToken resetToken = this.tokenRepository.findByToken(token);

        if (resetToken == null) {
            return new ResponseEntity<>(new ApiResponse("ERROR", "Nieprawidłowy token"), HttpStatus.UNAUTHORIZED);
        }

        if (isTokenExpired(resetToken)) {
            return new ResponseEntity<>(new ApiResponse("ERROR", "Token wygasł"), HttpStatus.UNAUTHORIZED);
        }

        String password = request.getNewPassword();
        String confirmPassword = request.getConfirmPassword();

        if (!password.equals(confirmPassword)) {
            return new ResponseEntity<>(new ApiResponse("ERROR", "Hasła są niezgodne. Spróbuj ponownie."), HttpStatus.BAD_REQUEST);
        }

        User user = resetToken.getUser();
        user.setPassword(this.passwordEncoder.encode(password));

        try {
            userRepository.save(user);
            List<PasswordResetToken> tokens = tokenRepository.findByUserId(user.getId());

            for (PasswordResetToken prtToken : tokens) {
                tokenRepository.delete(prtToken);
            }

            return ResponseEntity.ok(new ApiResponse("OK", "Poprawnie zaktualizowano hasło"));
        } catch (Exception e) {
            logger.error(e.getMessage());

            return new ResponseEntity<>(new ApiResponse("ERROR", "Wystąpił błąd"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private boolean isTokenExpired(PasswordResetToken passToken) {
        final Calendar cal = Calendar.getInstance();

        return passToken.getExpiryDate().before(cal.getTime());
    }
}
