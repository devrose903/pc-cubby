package com.example.pccubby.boundary.response;

import com.example.pccubby.entity.Hardware;

import java.time.LocalDateTime;

public class HardwareListResponse {
    private Integer id;
    private Boolean active;
    private String name;
    private String type;
    private String companyUser;
    private LocalDateTime created;

    public HardwareListResponse(Hardware hardware) {
        this.id = hardware.getId();
        this.active = hardware.getActive();
        this.name = hardware.getName();
        this.type = hardware.getType().getValue();
        this.companyUser = hardware.getCompanyUser();
        this.created = hardware.getCreated();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCompanyUser() {
        return companyUser;
    }

    public void setCompanyUser(String companyUser) {
        this.companyUser = companyUser;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
