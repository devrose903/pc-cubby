package com.example.pccubby.boundary.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserEditRequest {

    @NotNull(message = "Imie i nazwisko użytkownika nie może być puste")
    @Size(max = 155, message = "Zbyt długa nazwa sprzętu")
    private String name;

    @NotNull(message = "Login nie może być pusty")
    @Size(min = 3, max = 100, message = "Login ma niewłaściwą długość (minimum 3 znaki, max 100 znaków)")
    private String login;

    @Email
    @NotNull(message = "Nazwa nie może być pusta")
    @Size(max = 100, message = "Zbyt długa nazwa emaila")
    private String mail;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
