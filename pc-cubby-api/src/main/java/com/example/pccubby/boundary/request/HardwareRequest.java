package com.example.pccubby.boundary.request;

import com.example.pccubby.entity.enums.HardwareTypeEnum;
import org.hibernate.validator.constraints.URL;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.*;
import java.time.LocalDate;

public class HardwareRequest {
    private Integer id;

    @NotNull(message = "Nazwa nie może być pusta")
    @Size(max = 255, message = "Zbyt długa nazwa sprzętu")
    private String name;

    @NotNull(message = "Flaga czy sprzęt jest aktualny nie może być pusta")
    private Boolean active;

    @NotNull(message = "Typ nie może być pusty")
    @Enumerated(EnumType.STRING)
    private HardwareTypeEnum type;

    @Size(max = 255, message = "Użytkownik ma zbyt długą nazwe")
    private String companyUser;

    private String description;

    @PastOrPresent(message = "Data zakupu musi być datą bieżącą lub z przeszłości")
    private LocalDate buyDate;

    @NotNull(message = "Flaga czy aktywna gwarancja nie może być pusta")
    private Boolean activeGuarantee;

    @PositiveOrZero
    private Integer guaranteeYears;

    @Pattern(regexp = "^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})|([0-9a-fA-F]{4}\\\\.[0-9a-fA-F]{4}\\\\.[0-9a-fA-F]{4})$", message = "Podany adres MAC ma niewłaściwy format")
    private String macAddress;

    @NotNull(message = "Numer seryjny nie może być pusty")
    @Size(max = 100, message = "Numer seryjny: Zbyt długa nazwa")
    private String serialNum;

    @Size(max = 100, message = "Numer seryjny: Zbyt długa nazwa")
    private String invoiceNum;

    @Size(max = 50, message = "Numer seryjny: Zbyt długa nazwa")
    private String internalNum;

    @URL
    private String shopUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HardwareTypeEnum getType() {
        return type;
    }

    public void setType(HardwareTypeEnum type) {
        this.type = type;
    }

    public String getCompanyUser() {
        return companyUser;
    }

    public void setCompanyUser(String companyUser) {
        this.companyUser = companyUser;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(LocalDate buyDate) {
        this.buyDate = buyDate;
    }

    public Boolean getActiveGuarantee() {
        return activeGuarantee;
    }

    public void setActiveGuarantee(Boolean activeGuarantee) {
        this.activeGuarantee = activeGuarantee;
    }

    public Integer getGuaranteeYears() {
        return guaranteeYears;
    }

    public void setGuaranteeYears(Integer guaranteeYears) {
        this.guaranteeYears = guaranteeYears;
    }

    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    public String getInternalNum() {
        return internalNum;
    }

    public void setInternalNum(String internalNum) {
        this.internalNum = internalNum;
    }

    public String getShopUrl() {
        return shopUrl;
    }

    public void setShopUrl(String shopUrl) {
        this.shopUrl = shopUrl;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }
}
