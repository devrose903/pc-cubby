package com.example.pccubby.boundary.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ResetPasswordRequest {
    @NotNull(message = "Hasło nie może być puste")
    private String confirmPassword;

    @NotNull(message = "Token nie może być pusty")
    private  String token;

    @NotNull(message = "Hasło nie może być puste")
    @Size(min = 8, message = "Hasło powinno mieć przynajmniej znaków")
    private String newPassword;

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
