package com.example.pccubby.boundary.response;

import com.example.pccubby.entity.User;

public class UserSimplifiedResponse {
    private String login;

    private String name;

    private String mail;

    public UserSimplifiedResponse(User user) {
        this.login = user.getLogin();
        this.name = user.getName();
        this.mail = user.getMail();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
