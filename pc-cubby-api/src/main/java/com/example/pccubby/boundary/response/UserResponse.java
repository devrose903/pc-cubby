package com.example.pccubby.boundary.response;

import com.example.pccubby.entity.User;

import java.time.LocalDateTime;

public class UserResponse {
    private Integer id;
    private Boolean active;
    private String name;
    private String login;
    private String mail;
    private LocalDateTime created;
    private Boolean isAdmin;

    public UserResponse(User user) {
        this.id = user.getId();
        this.active = user.isActive();
        this.name = user.getName();
        this.login = user.getLogin();
        this.mail = user.getMail();
        this.created = user.getCreated();
        this.isAdmin = user.getRoles().stream().anyMatch(r -> r.getName().name().equals("ROLE_ADMIN"));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }
}
