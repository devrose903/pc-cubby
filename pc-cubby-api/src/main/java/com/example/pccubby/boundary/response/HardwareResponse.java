package com.example.pccubby.boundary.response;

import com.example.pccubby.entity.Hardware;
import com.example.pccubby.entity.HardwareDetails;

import java.time.LocalDate;

public class HardwareResponse {
    private Integer id;
    private Boolean active;
    private String name;
    private String type;
    private String typeTranslation;
    private String companyUser;
    private String description;
    private LocalDate buyDate;
    private Boolean activeGuarantee;
    private Integer guaranteeYears;
    private String serialNum;
    private String invoiceNum;
    private String internalNum;
    private String shopUrl;
    private String macAddress;

    public HardwareResponse(Hardware hardware, HardwareDetails details) {
        this.id = hardware.getId();
        this.active = hardware.getActive();
        this.name = hardware.getName();
        this.type = hardware.getType().name();
        this.typeTranslation = hardware.getType().getValue();
        this.companyUser = hardware.getCompanyUser();
        this.description = details.getDescription();
        this.buyDate = details.getBuyDate();
        this.activeGuarantee = details.getActiveGuarantee();
        this.guaranteeYears = details.getGuaranteeYears();
        this.serialNum = details.getSerialNum();
        this.invoiceNum = details.getInvoiceNum();
        this.internalNum = details.getInternalNum();
        this.shopUrl = details.getShopUrl();
        this.macAddress = details.getMacAddress();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeTranslation() {
        return typeTranslation;
    }

    public void setTypeTranslation(String typeTranslation) {
        this.typeTranslation = typeTranslation;
    }

    public String getCompanyUser() {
        return companyUser;
    }

    public void setCompanyUser(String companyUser) {
        this.companyUser = companyUser;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(LocalDate buyDate) {
        this.buyDate = buyDate;
    }

    public Boolean getActiveGuarantee() {
        return activeGuarantee;
    }

    public void setActiveGuarantee(Boolean activeGuarantee) {
        this.activeGuarantee = activeGuarantee;
    }

    public Integer getGuaranteeYears() {
        return guaranteeYears;
    }

    public void setGuaranteeYears(Integer guaranteeYears) {
        this.guaranteeYears = guaranteeYears;
    }

    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    public String getInternalNum() {
        return internalNum;
    }

    public void setInternalNum(String internalNum) {
        this.internalNum = internalNum;
    }

    public String getShopUrl() {
        return shopUrl;
    }

    public void setShopUrl(String shopUrl) {
        this.shopUrl = shopUrl;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }
}
