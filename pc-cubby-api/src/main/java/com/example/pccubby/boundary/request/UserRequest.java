package com.example.pccubby.boundary.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserRequest {
    @NotNull(message = "Pole aktywny nie może być puste")
    private Boolean active;

    @NotNull(message = "Imie i nazwisko użytkownika nie może być puste")
    @Size(max = 155, message = "Zbyt długa nazwa sprzętu")
    private String name;

    @NotNull(message = "Login nie może być pusty")
    @Size(min = 3, max = 100, message = "Login ma niewłaściwą długość (minimum 3 znaki, max 100 znaków)")
    @Pattern(regexp = "^([0-9A-a-Z-z_\\-@])+$", message = "Login powinien zawierać tylko znaki alfanumeryczne oraz znaki specjalne '-' lub '_'")
    private String login;

    @NotNull(message = "Hasło nie może być puste")
    @Size(min = 8, message = "Hasło powinno mieć przynajmniej znaków")
    private String password;

    @Email
    @NotNull(message = "Nazwa nie może być pusta")
    @Size(max = 100, message = "Zbyt długa nazwa emaila")
    private String mail;

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
