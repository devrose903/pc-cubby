package com.example.pccubby.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Access;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Entity
@Table(name = "hardware_details")
@JsonIgnoreProperties(ignoreUnknown = true, allowGetters = true, value = {"created", "modified"})
public class HardwareDetails {
    @Access(AccessType.PROPERTY)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(columnDefinition = "text")
    private String description;

    @Column(name = "buy_date")
    private LocalDate buyDate;

    @Column(name = "active_guarantee")
    private Boolean activeGuarantee;

    @Column(name = "guarantee_years")
    private Integer guaranteeYears;

    @Column(name = "mac_address")
    private String macAddress;

    @NotBlank
    @Column(name = "serial_num")
    private String serialNum;

    @Column(name = "invoice_num")
    private String invoiceNum;

    @Column(name = "internal_num")
    private String internalNum;

    @Column(name = "shop_url", columnDefinition = "text")
    private String shopUrl;

    public HardwareDetails() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(LocalDate buyDate) {
        this.buyDate = buyDate;
    }

    public Boolean getActiveGuarantee() {
        return activeGuarantee;
    }

    public void setActiveGuarantee(Boolean activeGuarantee) {
        this.activeGuarantee = activeGuarantee;
    }

    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    public String getInternalNum() {
        return internalNum;
    }

    public void setInternalNum(String internalNum) {
        this.internalNum = internalNum;
    }

    public String getShopUrl() {
        return shopUrl;
    }

    public void setShopUrl(String shopUrl) {
        this.shopUrl = shopUrl;
    }

    public Integer getGuaranteeYears() {
        return guaranteeYears;
    }

    public void setGuaranteeYears(Integer guaranteeYears) {
        this.guaranteeYears = guaranteeYears;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }
}
