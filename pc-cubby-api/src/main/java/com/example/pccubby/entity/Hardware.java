package com.example.pccubby.entity;

import com.example.pccubby.entity.enums.HardwareTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Entity
@Table(name = "hardware",
        indexes = {@Index(name = "hardware_user_idx", columnList = "user_id")},
        uniqueConstraints = {@UniqueConstraint(name = "hardware_name_user_idx", columnNames = {"name", "user_id"})})
@JsonIgnoreProperties(
        value = {"created", "modified"},
        allowGetters = true,
        ignoreUnknown = true
)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Hardware {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "Wartość nie może być pusta")
    @Column(columnDefinition = "text")
    private String name;

    @Column(name = "active")
    private Boolean active;

    @CreatedDate
    private LocalDateTime created;

    @Version
    @LastModifiedDate
    private LocalDateTime modified;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private HardwareTypeEnum type;

    @Column(name = "company_user")
    @Size(max = 155, message = "Zbyt długa nazwa użytkownika sprzętu")
    private String companyUser;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "details_id", nullable = false)
    private HardwareDetails details;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @PrePersist
    void prePersist() {
        this.created = LocalDateTime.now();
    }

    @PreUpdate
    void preUpdate() {
        this.modified = LocalDateTime.now();
    }

    public Hardware() {

    }

    public Hardware(String name) {
        this.name = name;
    }

    //---------------------------auto-generated---------------------

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    public HardwareTypeEnum getType() {
        return type;
    }

    public void setType(HardwareTypeEnum type) {
        this.type = type;
    }

    public String getCompanyUser() {
        return companyUser;
    }

    public void setCompanyUser(String companyUser) {
        this.companyUser = companyUser;
    }

    public HardwareDetails getDetails() {
        return details;
    }

    public void setDetails(HardwareDetails details) {
        this.details = details;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
