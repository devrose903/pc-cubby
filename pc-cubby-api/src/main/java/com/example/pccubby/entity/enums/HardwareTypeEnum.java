package com.example.pccubby.entity.enums;

public enum HardwareTypeEnum {
    MONITOR("Monitor"),
    MOUSE("Myszka"),
    LAPTOP("Laptop"),
    KEYBOARD("Klawiatura"),
    CPU("Procesor"),
    GRAPHICS_CARD("Karta graficzna"),
    MOTHERBOARD("Płyta główna"),
    RAM ("RAM"),
    DISC("Dysk SSD/HDD"),
    CASE("Obudowa"),
    HEADPHONES("Słuchawki"),
    CAMERA("Kamera"),
    ADAPTER("Adapter"),
    OTHER("Inny");

    private final String value;

    HardwareTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
