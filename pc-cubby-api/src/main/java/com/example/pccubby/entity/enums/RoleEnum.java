package com.example.pccubby.entity.enums;

public enum RoleEnum {
    ROLE_ADMIN,
    ROLE_USER
}
