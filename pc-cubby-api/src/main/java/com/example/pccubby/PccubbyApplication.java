package com.example.pccubby;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PccubbyApplication {

	public static void main(String[] args) {
		SpringApplication.run(PccubbyApplication.class, args);
	}

}