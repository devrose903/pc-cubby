import auth from '@/vuex/modules/auth'
import { ENDPOINTS } from '@/const/endpoints.const'
import { HARDWARE_DETAILS_MODEL } from '@/const/model.const'
import { ToolsClass } from '@/class/tools.class'
import { apiClass } from '@/class/api.class'
import store from '@/vuex/store'

const state = {
  data: ToolsClass.deepExtend(HARDWARE_DETAILS_MODEL),
  processing: false,
  ref: 'hardwareForm',
  optionsLoading: false,
  options: {
    typeList: null
  }
}

const mutations = {
  SET_HARDWARE_FORM_PROCESSING (state, value) {
    state.processing = value
  },
  REFRESH_HARDWARE_FORM_DATA (state) {
    state.data = ToolsClass.deepExtend(HARDWARE_DETAILS_MODEL)
    state.processing = false
  },
  SET_HARDWARE_SELECT_TYPES (state, value) {
    state.options.typeList = value
  },
  SET_FORM_SELECT_LOADING (state, value) {
    state.optionsLoading = value
  },
  SET_FORM_HARDWARE_NAME (state, value) {
    state.data.name = value
  },
  SET_FORM_HARDWARE_ACTIVE (state, value) {
    state.data.active = value
  },
  SET_FORM_HARDWARE_TYPE (state, value) {
    state.data.type = value
  },
  SET_FORM_HARDWARE_COMPANY_USER (state, value) {
    state.data.companyUser = value
  },
  SET_FORM_HARDWARE_DESC (state, value) {
    state.data.description = value
  },
  SET_FORM_HARDWARE_BUY_DATE (state, value) {
    state.data.buyDate = value
  },
  SET_FORM_HARDWARE_ACTIVE_GUARANTEE (state, value) {
    state.data.activeGuarantee = value
  },
  SET_FORM_HARDWARE_GUARANTEE_YEARS (state, value) {
    state.data.guaranteeYears = value
  },
  SET_FORM_HARDWARE_SERIAL_NUM (state, value) {
    state.data.serialNum = value
  },
  SET_FORM_HARDWARE_INVOICE_NUM (state, value) {
    state.data.invoiceNum = value
  },
  SET_FORM_HARDWARE_INTERNAL_NUM (state, value) {
    state.data.internalNum = value
  },
  SET_FORM_HARDWARE_SHOP_URL (state, value) {
    state.data.shopUrl = value
  },
  SET_FORM_HARDWARE_MACADRESS (state, value) {
    state.data.macAddress = value
  },
  SET_HARDWARE_EDIT_DATA (state, data) {
    state.data = ToolsClass.deepExtend(data)
  }
}

const actions = {
  getHardwareTypesFromApi (context) {
    context.commit('SET_FORM_SELECT_LOADING', true)

    return apiClass.get(ENDPOINTS.hardware.select.type, auth.getters.getAuthHeader(store.state.auth))
      .then(response => {
        context.commit('SET_HARDWARE_SELECT_TYPES', response.body)

        return Promise.resolve()
      })
      .catch(error => {
        return Promise.reject(error)
      })
      .finally(() => {
        context.commit('SET_FORM_SELECT_LOADING', false)
      })
  },
  addHardwareData (context) {
    context.commit('SET_HARDWARE_FORM_PROCESSING', true)

    const data = ToolsClass.deepExtend(HARDWARE_DETAILS_MODEL)
    data.active = context.state.data.active
    data.name = context.state.data.name
    data.type = context.state.data.type
    data.companyUser = context.state.data.companyUser
    data.description = context.state.data.description
    data.buyDate = ToolsClass.getFormattedDate(context.state.data.buyDate, 'Y-m-d')
    data.activeGuarantee = context.state.data.activeGuarantee
    data.guaranteeYears = context.state.data.guaranteeYears
    data.serialNum = context.state.data.serialNum
    data.invoiceNum = context.state.data.invoiceNum
    data.internalNum = context.state.data.internalNum
    data.macAddress = context.state.data.macAddress
    data.shopUrl = context.state.data.shopUrl

    return apiClass.post(ENDPOINTS.hardware.create, data, auth.getters.getAuthHeader(store.state.auth))
      .then(() => {
        return Promise.resolve()
      })
      .catch(error => {
        return Promise.reject(error)
      })
      .finally(() => {
        context.commit('SET_HARDWARE_FORM_PROCESSING', false)
      })
  },
  updateHardwareData (context) {
    context.commit('SET_HARDWARE_FORM_PROCESSING', true)

    return apiClass.post(ENDPOINTS.hardware.update, state.data, auth.getters.getAuthHeader(store.state.auth))
      .then(() => {
        return Promise.resolve(state.data.id)
      })
      .catch(error => {
        return Promise.reject(error)
      })
      .finally(() => {
        context.commit('SET_HARDWARE_FORM_PROCESSING', false)
      })
  }
}

const getters = {
  getTypesList (state) {
    if (!state.options.typeList) {
      return []
    }
    return state.options.typeList
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
