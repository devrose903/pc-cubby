import auth from '@/vuex/modules/auth'
import { ENDPOINTS } from '@/const/endpoints.const'
import { USER_MODEL } from '@/const/model.const'
import { ToolsClass } from '@/class/tools.class'
import { apiClass } from '@/class/api.class'
import store from '@/vuex/store'

const state = {
  data: ToolsClass.deepExtend(USER_MODEL),
  processing: false,
  ref: 'userForm'
}

const mutations = {
  SET_USER_FORM_PROCESSING (state, value) {
    state.processing = value
  },
  REFRESH_USER_FORM_DATA (state) {
    state.data = ToolsClass.deepExtend(USER_MODEL)
    state.processing = false
  },
  SET_USER_EDIT_DATA (state, data) {
    state.data = ToolsClass.deepExtend(data)
  },
  SET_USER_ACTIVE (state, value) {
    state.data.active = value
  },
  SET_USER_NAME (state, value) {
    state.data.name = value
  },
  SET_USER_LOGIN (state, value) {
    state.data.login = value
  },
  SET_USER_MAIL (state, value) {
    state.data.mail = value
  },
  SET_USER_PASSWORD (state, value) {
    if (value === '') {
      state.data.password = null
    } else {
      state.data.password = value
    }
  }
}

const actions = {
  createUser (context) {
    context.commit('SET_USER_FORM_PROCESSING', true)

    const data = ToolsClass.deepExtend(USER_MODEL)
    data.name = context.state.data.name
    data.login = context.state.data.login
    data.mail = context.state.data.mail
    data.password = context.state.data.password

    return apiClass.post(ENDPOINTS.user.create, data, auth.getters.getAuthHeader(store.state.auth))
      .then(() => {
        return Promise.resolve()
      })
      .catch(error => {
        return Promise.reject(error)
      })
      .finally(() => {
        context.commit('SET_USER_FORM_PROCESSING', false)
      })
  },
  updateUser (context) {
    context.commit('SET_USER_FORM_PROCESSING', true)

    let data = {
      login: context.state.data.login,
      name: context.state.data.name,
      mail: context.state.data.mail
    }

    return apiClass.post(ENDPOINTS.user.update, data, auth.getters.getAuthHeader(store.state.auth))
      .then(response => {
        return Promise.resolve(response)
      })
      .catch(error => {
        return Promise.reject(error)
      })
      .finally(() => {
        context.commit('SET_USER_FORM_PROCESSING', false)
      })
  },
  updateSimplyUser (context) {
    context.commit('SET_USER_FORM_PROCESSING', true)

    let data = {
      login: context.state.data.login,
      name: context.state.data.name,
      mail: context.state.data.mail,
      password: context.state.data.password
    }

    return apiClass.post(ENDPOINTS.user.simply_update, data, auth.getters.getAuthHeader(store.state.auth))
      .then(response => {
        return Promise.resolve(response)
      })
      .catch(error => {
        return Promise.reject(error)
      })
      .finally(() => {
        context.commit('SET_USER_FORM_PROCESSING', false)
      })
  },
  getSimplifiedUserInfo (context) {
    return apiClass.get(ENDPOINTS.user.info, auth.getters.getAuthHeader(store.state.auth))
      .then(response => {
        context.commit('SET_USER_EDIT_DATA', response.body)
        return Promise.resolve()
      })
      .catch(error => {
        return Promise.reject(error)
      })
  }
}

const getters = {}

export default {
  state,
  mutations,
  actions,
  getters
}
