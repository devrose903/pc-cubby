import { apiClass } from '@/class/api.class'
import { ToolsClass } from '@/class/tools.class'
import auth from '@/vuex/modules/auth'
import { ENDPOINTS } from '@/const/endpoints.const'
import store from '@/vuex/store'

const state = {
  loading: false,
  items: null,
  processing: false,
  details: null,
  filters: {
    name: null,
    companyUser: null
  }
}

const mutations = {
  SET_HARDWARE_LIST_LOADING (state, value) {
    state.loading = value
  },
  SET_HARDWARE_ITEMS (state, data) {
    state.items = ToolsClass.deepExtend(data)
  },
  SET_HARDWARE_PROCESSING (state, value) {
    state.processing = value
  },
  SET_HARDWARE_DETAILS (state, value) {
    state.details = value
  },
  SET_HARDWARE_FILTERS_NAME (state, value) {
    if (value === '') {
      value = null
    }
    state.filters.name = value
  },
  SET_HARDWARE_FILTERS_COMPANY_USER (state, value) {
    if (value === '') {
      value = null
    }
    state.filters.companyUser = value
  }
}

const actions = {
  getHardwareListFromApi (context) {
    context.commit('SET_HARDWARE_LIST_LOADING', true)

    return apiClass.post(ENDPOINTS.hardware.list, state.filters, auth.getters.getAuthHeader(store.state.auth))
      .then(response => {
        context.commit('SET_HARDWARE_ITEMS', response.body)
        return Promise.resolve()
      })
      .catch(error => {
        return Promise.reject(error)
      })
      .finally(() => {
        context.commit('SET_HARDWARE_LIST_LOADING', false)
      })
  },
  getHardwareDetailsFromApi (context, hardwareId) {
    context.commit('SET_HARDWARE_LIST_LOADING', true)
    const url = ENDPOINTS.hardware.details.replace('{ID}', hardwareId)

    return apiClass.get(url, auth.getters.getAuthHeader(store.state.auth))
      .then(response => {
        context.commit('SET_HARDWARE_DETAILS', response.body)
        return Promise.resolve()
      })
      .catch(error => {
        return Promise.reject(error)
      })
      .finally(() => {
        context.commit('SET_HARDWARE_LIST_LOADING', false)
      })
  },
  removeHardware (context, hardwareId) {
    const url = ENDPOINTS.hardware.remove.replace('{ID}', hardwareId)

    apiClass.delete(url, {}, auth.getters.getAuthHeader(store.state.auth))
      .then(() => {
        return Promise.resolve()
      })
      .catch(error => {
        return Promise.reject(error)
      })
  }
}

const getters = {
  getHardwareList (state) {
    if (!state.items) {
      return []
    }
    return state.items
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
