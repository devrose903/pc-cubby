import { apiClass } from '@/class/api.class'
import { ENDPOINTS } from '@/const/endpoints.const'

const state = {
  authenticated: false,
  userJwt: null
}

const mutations = {
  SET_AUTHENTICATED (state, jwtResponse) {
    state.userJwt = jwtResponse
    state.authenticated = true
  },
  CLEAR_AUTHENTICATION (state) {
    state.userJwt = null
    state.authenticated = false
  }
}

const actions = {
  authenticate (context, credentials) {
    return apiClass.post(ENDPOINTS.auth.login, credentials)
      .then(response => {
        if (response.body.accessToken !== 'undefined') {
          context.commit('SET_AUTHENTICATED', response.body)
          return Promise.resolve()
        }
        return Promise.reject(response)
      }, error => {
        return Promise.reject(error)
      })
  },
  clearAuthentication (context) {
    return apiClass.get(ENDPOINTS.auth.logout, context.getters.getAuthHeader)
      .then(() => {
        context.commit('CLEAR_AUTHENTICATION')
        return Promise.resolve()
      }, error => {
        return Promise.reject(error)
      })
  }
}

const getters = {
  getAuthHeader (state) {
    return {
      headers: {
        'Authorization': `Bearer ${state.userJwt.accessToken}`
      }
    }
  },
  hasAdminAuthority (state) {
    return state.userJwt.authorities[0] === 'ROLE_ADMIN'
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
