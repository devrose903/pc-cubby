import { apiClass } from '@/class/api.class'
import { ToolsClass } from '@/class/tools.class'
import auth from '@/vuex/modules/auth'
import { ENDPOINTS } from '@/const/endpoints.const'
import store from '@/vuex/store'

const state = {
  loading: false,
  items: null,
  processing: false
}

const mutations = {
  SET_USER_LIST_LOADING (state, value) {
    state.loading = value
  },
  SET_USER_ITEMS (state, data) {
    state.items = ToolsClass.deepExtend(data)
  },
  SET_USER_PROCESSING (state, value) {
    state.processing = value
  }
}

const actions = {
  getUserListFromApi (context) {
    context.commit('SET_USER_LIST_LOADING', true)

    return apiClass.get(ENDPOINTS.user.list, auth.getters.getAuthHeader(store.state.auth))
      .then(response => {
        context.commit('SET_USER_ITEMS', response.body)
        return Promise.resolve()
      })
      .catch(error => {
        return Promise.reject(error)
      })
      .finally(() => {
        context.commit('SET_USER_LIST_LOADING', false)
      })
  },
  deactivateUser (context, username) {
    context.commit('SET_USER_LIST_LOADING', true)

    const url = ENDPOINTS.user.deactivate.replace('{username}', username)

    return apiClass.post(url, {}, auth.getters.getAuthHeader(store.state.auth))
      .then(() => {
        return Promise.resolve()
      })
      .catch(error => {
        return Promise.reject(error)
      })
      .finally(() => {
        context.commit('SET_USER_LIST_LOADING', false)
      })
  },
  activateUser (context, username) {
    context.commit('SET_USER_LIST_LOADING', true)

    const url = ENDPOINTS.user.activate.replace('{username}', username)

    return apiClass.post(url, {}, auth.getters.getAuthHeader(store.state.auth))
      .then(() => {
        return Promise.resolve()
      })
      .catch(error => {
        return Promise.reject(error)
      })
      .finally(() => {
        context.commit('SET_USER_LIST_LOADING', false)
      })
  }
}

const getters = {
  getUserList (state) {
    if (!state.items) {
      return []
    }
    return state.items
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
