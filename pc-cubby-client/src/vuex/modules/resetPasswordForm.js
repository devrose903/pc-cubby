import { apiClass } from '@/class/api.class'
import { ToolsClass } from '@/class/tools.class'
import { RESET_PASSWORD_SEND_MODEL, RESET_PASSWORD_UPDATE_MODEL } from '@/const/model.const'
import { ENDPOINTS } from '@/const/endpoints.const'

const state = {
  dataForSend: ToolsClass.deepExtend(RESET_PASSWORD_SEND_MODEL),
  dataForUpdate: ToolsClass.deepExtend(RESET_PASSWORD_UPDATE_MODEL),
  processing: false,
  refSend: 'resetPasswordForm',
  refUpdate: 'resetPasswordUpdateForm'
}

const mutations = {
  SET_RESET_PASS_FORM_PROCESSING (state, value) {
    state.processing = value
  },
  REFRESH_RESET_PASS_FORM_SEND_DATA (state) {
    state.dataForSend = ToolsClass.deepExtend(RESET_PASSWORD_SEND_MODEL)
    state.processing = false
  },
  REFRESH_RESET_PASS_FORM_UPDATE_DATA (state) {
    state.dataForUpdate = ToolsClass.deepExtend(RESET_PASSWORD_UPDATE_MODEL)
    state.processing = false
  },
  SET_RESET_PASS_LOGIN (state, value) {
    state.dataForSend.login = value
  },
  SET_RESET_PASS_TOKEN (state, value) {
    state.dataForUpdate.token = value
  },
  SET_RESET_PASS_PASSWORD (state, value) {
    state.dataForUpdate.newPassword = value
  },
  SET_RESET_PASS_PASSWORD_CONFIRM (state, value) {
    state.dataForUpdate.confirmPassword = value
  }
}

const actions = {
  sendEmailForPasswordReset (context) {
    context.commit('SET_USER_FORM_PROCESSING', true)

    const url = ENDPOINTS.auth.reset_password.send.replace('{login}', context.state.dataForSend.login)

    return apiClass.post(url, {})
      .then(() => {
        return Promise.resolve()
      })
      .catch(error => {
        return Promise.reject(error)
      })
      .finally(() => {
        context.commit('SET_USER_FORM_PROCESSING', false)
      })
  },
  updateUserPassword (context) {
    context.commit('SET_USER_FORM_PROCESSING', true)

    const data = ToolsClass.deepExtend(RESET_PASSWORD_UPDATE_MODEL)
    data.newPassword = context.state.dataForUpdate.newPassword
    data.confirmPassword = context.state.dataForUpdate.confirmPassword
    data.token = state.dataForUpdate.token

    return apiClass.post(ENDPOINTS.auth.reset_password.update, data)
      .then(() => {
        return Promise.resolve()
      })
      .catch(error => {
        return Promise.reject(error)
      })
      .finally(() => {
        context.commit('SET_USER_FORM_PROCESSING', false)
      })
  }
}

const getters = {}

export default {
  state,
  mutations,
  actions,
  getters
}
