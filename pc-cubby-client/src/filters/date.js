import { ToolsClass } from '@/class/tools.class'

export default function (date, format = 'Y-m-d H:i:s') {
  if (!date) return '-'

  if (!(date instanceof Date)) {
    date = new Date(date)
  }
  return ToolsClass.getFormattedDate(date, format)
}
