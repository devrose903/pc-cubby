import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './vuex/store'
import VueResource from 'vue-resource'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/pl'
import 'element-ui/lib/theme-chalk/index.css'
import VTooltip from 'v-tooltip'
import filterDate from './filters/date'

Vue.config.productionTip = false

Vue.use(ElementUI, { locale })
Vue.use(VueResource)
Vue.use(VTooltip)
Vue.filter('date', filterDate)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
