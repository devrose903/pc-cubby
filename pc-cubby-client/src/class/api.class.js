import Vue from 'vue'

const OPTIONS = {
  credentials: true
}

const STATUS_OK = 200

function isResponseValid (response) {
  return response.status === STATUS_OK && (isResponseValidObject(response) || isResponseValidString(response))
}
function isResponseValidString (response) {
  return typeof response.body === 'string'
}
function isResponseValidObject (response) {
  return typeof response.body === 'object'
}

export class ApiClass {
  get (endpoint, options = {}) {
    Object.assign(options, OPTIONS)

    let url = ApiClass.getBackendUrl(endpoint)

    return Vue.http.get(url, options)
      .then(response => {
        return Promise.resolve(response)
      })
      .catch(error => {
        return Promise.reject(error)
      })
  }
  post (endpoint, data = {}, options = {}) {
    Object.assign(options, OPTIONS)

    let url = ApiClass.getBackendUrl(endpoint)

    return Vue.http.post(url, data, options)
      .then(response => {
        if (!isResponseValid(response)) {
          return Promise.reject(response)
        }
        return Promise.resolve(response)
      })
      .catch(error => {
        return Promise.reject(error)
      })
  }
  delete (endpoint, data = {}, options = {}) {
    Object.assign(options, OPTIONS)
    Object.assign(options, { body: data })

    let url = ApiClass.getBackendUrl(endpoint)

    return Vue.http.delete(url, options)
      .then(response => {
        if (!isResponseValid(response)) {
          return Promise.reject(response)
        }

        return Promise.resolve(response)
      })
      .catch(error => {
        return Promise.reject(error)
      })
  }

  static getBackendUrl (endpoint) {
    return process.env.VUE_APP_API + endpoint
  }
}

export let apiClass = new ApiClass()
