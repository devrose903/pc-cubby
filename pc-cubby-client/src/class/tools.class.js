export class ToolsClass {
  static deepExtend (value, dest, key) {
    if (value instanceof Array) {
      let newArr = []
      value.forEach((arrValue, index) => {
        this.deepExtend(arrValue, newArr, index)
      })
      addResult(newArr)
    } else if (value instanceof Date) {
      addResult(new Date(value))
    } else if (value instanceof Object) {
      let newObj = {}
      for (let key2 in value) {
        this.deepExtend(value[key2], newObj, key2)
      }
      addResult(newObj)
    } else {
      addResult(value)
    }
    return dest

    function addResult (result) {
      if (typeof dest === 'undefined') {
        dest = result
      } else {
        dest[key] = result
      }
    }
  }

  static getFormattedDate (date, format = 'Y-m-d H:i:s') {
    if (!date || !(date instanceof Date)) {
      return
    }

    let Y = date.getFullYear().toString()
    let m = ('0' + (date.getMonth() + 1)).slice(-2)
    let d = ('0' + date.getDate()).slice(-2)
    let H = ('0' + date.getHours()).slice(-2)
    let i = ('0' + date.getMinutes()).slice(-2)
    let s = ('0' + date.getSeconds()).slice(-2)

    switch (format) {
      case 'Y-m-d H:i:s':
        return [Y, '-', m, '-', d, ' ', H, ':', i, ':', s].join('')

      case 'Y-m-d':
        return [Y, '-', m, '-', d].join('')

      case 'H:i:s':
        return [H, ':', i, ':', s].join('')

      case 'YmdHis':
      default:
        return [Y, m, d, H, i, s].join('')
    }
  }
}
