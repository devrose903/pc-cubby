export const ENDPOINTS = {
  auth: {
    login: 'api/auth/login',
    logout: 'api/auth/logout',
    reset_password: {
      send: 'api/auth/reset-password/send/{login}',
      update: 'api/auth/reset-password/update'
    }
  },
  user: {
    list: 'api/user/list',
    create: 'api/user/create',
    update: 'api/user/update',
    simply_update: 'api/user/update/simply',
    info: 'api/user/info',
    deactivate: 'api/user/deactivate/{username}',
    activate: 'api/user/activate/{username}'
  },
  hardware: {
    list: 'api/hardware/list',
    details: 'api/hardware/details/{ID}',
    create: 'api/hardware/create',
    update: 'api/hardware/update',
    remove: 'api/hardware/remove/{ID}',
    select: {
      type: 'api/hardware/select/types'
    }
  }
}
