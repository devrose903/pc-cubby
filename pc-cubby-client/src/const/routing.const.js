export const ROUTING = {
  login: '/login',
  hardwares: '/hardwares',
  users: '/users',
  reset_password: '/reset-password'
}
