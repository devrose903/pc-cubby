export const HARDWARE_DETAILS_MODEL = {
  id: null,
  active: true,
  name: null,
  type: null,
  companyUser: null,
  description: null,
  buyDate: null,
  activeGuarantee: false,
  guaranteeYears: 0,
  serialNum: null,
  invoiceNum: null,
  internalNum: null,
  shopUrl: null,
  macAddress: null
}

export const USER_MODEL = {
  active: true,
  name: null,
  login: null,
  mail: null,
  password: null
}

export const RESET_PASSWORD_SEND_MODEL = {
  login: null
}

export const RESET_PASSWORD_UPDATE_MODEL = {
  newPassword: null,
  confirmPassword: null,
  token: null
}
