import Vue from 'vue'
import Router from 'vue-router'
import store from '@/vuex/store'
import { ROUTING } from '@/const/routing.const'
import HardwaresView from '@/views/HardwaresView'
import LoginView from '@/views/LoginView'
import UsersView from '@/views/UsersView'
import HardwareDetailsView from '@/views/HardwareDetailsView'
import ResetPasswordSendView from '@/views/ResetPasswordSendView'
import ResetPasswordUpdate from '@/views/ResetPasswordUpdateView'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: ROUTING.login,
      component: LoginView
    },
    {
      path: ROUTING.reset_password,
      component: ResetPasswordSendView
    },
    {
      path: ROUTING.reset_password + '/:token',
      component: ResetPasswordUpdate,
      props: true
    },
    {
      path: ROUTING.hardwares,
      component: HardwaresView,
      beforeEnter (to, from, next) {
        if (store.state.auth.authenticated) {
          next()
        } else {
          next(ROUTING.login)
        }
      }
    },
    {
      path: ROUTING.hardwares + '/:id',
      component: HardwareDetailsView,
      props: true,
      beforeEnter (to, from, next) {
        if (store.state.auth.authenticated) {
          next()
        } else {
          next(ROUTING.login)
        }
      }
    },
    {
      path: ROUTING.users,
      component: UsersView,
      beforeEnter (to, from, next) {
        if (store.state.auth.authenticated && store.getters.hasAdminAuthority) {
          next()
        } else {
          next(ROUTING.hardwares)
        }
      }
    },
    {
      path: '*',
      redirect: ROUTING.hardwares
    }
  ]
})

export default router
