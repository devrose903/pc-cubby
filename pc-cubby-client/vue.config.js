module.exports = {
  configureWebpack: {
    devServer: {
      host: 'pc-cubby.local'
    }
  },
  css: {
    loaderOptions: {
      postcss: {
        exclude: /node_modules/
      }
    }
  }
}
