module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'prod' ? 'off' : 'off',
    'no-debugger': process.env.NODE_ENV === 'prod' ? 'error' : 'off'
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
